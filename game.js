
//Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 500;
canvas.height = 500;
document.body.appendChild(canvas);

//Game Objects
var hero = {
	image: new Image(),
	speed: 200, //pixels per second
	jumpSpeed: 500,
	xIndex: 0,
	yIndex: 0,
	x: 0,
	y: 100,
	direction: "right",
	isJumping: false,
	jumpFrames: 0
};

var background = {
	image: new Image()
};

var land = {
	image: new Image(),
	x: 0,
	y: 400
};

var monster = {
	image: new Image(),
	x: 0,
	y: 0
};

var coin = {
	context: canvas.getContext("2d"),
	xIndex: 0,
	yIndex: 0,
	width: 46,
	height: 100,
	image: new Image()
};

//Handle keyboard control
var keysDown = {};

addEventListener("keydown", function (e) {
	keysDown[e.keyCode] = true;
}, false);

addEventListener("keyup", function(e) {
	delete keysDown[e.keyCode];
}, false);


//Reset game when player catches monster
var reset = function() {
 
	hero.x = 0;
	hero.y = 50;
	
	//Throw the monster somewhere on the screen
	monster.x = 32 + (Math.random() * (canvas.width - 64));
	monster.y = 32 + (Math.random()) * (canvas.height - 64);
};

var checkPlatformCollision = function(gameObject, dt){

	//check collision with purple
	if (gameObject.isJumping == false){
		if ((gameObject.y) < land.y - 51||
			(gameObject.x > land.x + 500)) { // danger zone
			gameObject.y += gameObject.speed * dt;
		}
	}
	

};

var moveObject = function(gameObject, dt, direction){

	if (direction == "left"){ //move object left
	
		gameObject.direction = "left";
		gameObject.x -= gameObject.speed * dt;
		
	}else if (direction == "right"){ //move object right
	
		gameObject.direction = "right";
		gameObject.x += gameObject.speed * dt;
		
	}else{
		//don't move object
	}

};

var animateObject = function(gameObject, frame){

	if (frame >= 0 && frame < 8){
				gameObject.xIndex = 0;
				gameObject.yIndex = 101;
			}else if (frame >= 8 && frame < 16){
				gameObject.xIndex = 100;
				gameObject.yIndex = 101;
			}else if (frame >= 16 && frame < 24){
				gameObject.xIndex = 200;
				gameObject.yIndex = 101;
			}else if (frame >= 24 && frame < 32){
				gameObject.xIndex = 300;
				gameObject.yIndex = 101;
			}

};

	var frameCount = 0;

//Update Game objects
var update = function (modifier) {

	frameCount = frameCount % 32;

	if (37 in keysDown){ //holding left

		moveObject(hero, modifier, "left");
		animateObject(hero, frameCount);
		
	}else if (39 in keysDown){ //holding right
	
		moveObject(hero, modifier, "right");
		
		if (frameCount >= 0 && frameCount < 8){
			hero.xIndex = 0;
			hero.yIndex = 0;
		}else if (frameCount >= 8 && frameCount < 16){
			hero.xIndex = 100;
			hero.yIndex = 0;
		}else if (frameCount >= 16 && frameCount < 24){
			hero.xIndex = 200;
			hero.yIndex = 0;
		}else if (frameCount >= 24 && frameCount < 32){
			hero.xIndex = 300;
			hero.yIndex = 0;
		}
	
	}else if (32 in keysDown){ //space bar
		reset();
		
	}else{ //nothing is pressed
	
		if (hero.direction.localeCompare("left")){ //keep facing current direction
			hero.xIndex = 0;
			hero.yIndex = 0;
		}else{
			hero.xIndex = 0;
			hero.yIndex = 101;
		}
	
	}
	
	if (65 in keysDown && !hero.isJumping){
		hero.isJumping = true;
	
	}
	
	if (hero.isJumping){
	
		if (hero.jumpFrames < 20){
			hero.y -= hero.jumpSpeed * modifier;
		}else if (hero.jumpFrames >= 22 && hero.jumpFrames < 42){
			hero.y += hero.jumpSpeed * modifier;
		}
	
		hero.jumpFrames++;
		
		if (hero.jumpFrames >= 43){
		
			hero.jumpFrames = 0;
			hero.isJumping = false;
		}
	
	
	}
	
	checkPlatformCollision(hero, modifier);
	
	
	if (frameCount % 4 == 0){ //next sprite animation
		coin.xIndex += 44;
		
		if (coin.xIndex >= 440){
		coin.xIndex = 0;
		}
	
	}
	frameCount++;
};


// Draw everything
var render = function () {
	
		ctx.drawImage(background.image, 0, 0, 500, 500);
	
		ctx.drawImage(hero.image, hero.xIndex, hero.yIndex, 100, 100, hero.x, hero.y, 50, 50);
	
		ctx.drawImage(land.image, land.x, land.y, 500, 100);
		//ctx.drawImage(landImage, land.x + 200, land.y);
		
		ctx.drawImage(coin.image, coin.xIndex, 0, coin.width, coin.height, 50, 100, coin.width/2, coin.height/2);
		ctx.drawImage(coin.image, coin.xIndex, 0, coin.width, coin.height, 100, 100, coin.width/2, coin.height/2);
		ctx.drawImage(coin.image, coin.xIndex, 0, coin.width, coin.height, 150, 100, coin.width/2, coin.height/2);
		ctx.drawImage(coin.image, coin.xIndex, 0, coin.width, coin.height, 200, 100, coin.width/2, coin.height/2);
		
	/*
	if (monsterReady) {
		ctx.drawImage(monsterImage, monster.x, monster.y);
	}
	**/

	// Score
	ctx.fillStyle = "rgb(250, 250, 250)";
	ctx.font = "24px Helvetica";
	ctx.textAlign = "right";
	ctx.textBaseline = "top";
	ctx.fillText("Score: 0",500, 10);
};
	
var loadResources = function() {

	var bgImage = background.image;
	var heroImage = hero.image;
	var landImage = land.image;
	var coinImage = coin.image;
	
	var totalResources  = 4;
	var loadedResources = 0;
	
	var images = [];
	
	bgImage.src = "img/sky.png";
	heroImage.src = "img/hatmanSprite.png";
	landImage.src = "img/purple.jpg";
	coinImage.src = "img/coin.png";
	
	images.push(bgImage);
	images.push(heroImage);
	images.push(landImage);
	images.push(coinImage);

	for (x in images){
			loadedResources++;
	}
	
	if (loadedResources != totalResources){
		alert("Resources not loaded correctly");
	}
};

var now, 
	dt = 0,
	last = timestamp(),
	step = 1/60;

var fpsmeter = new FPSMeter({ decimals: 0, graph: true, theme: 'dark', left: '5px' });

// The main game loop
var main = function () {
	fpsmeter.tickStart();
	
	now = timestamp();
	dt = dt + Math.min(1, (now - last) / 1000);  //seconds between last frame and this frame capped to 1 second
	
	while (dt > step) {
		dt = dt - step;
		update(step);
	}
	
	render();
	last = now;
	fpsmeter.tick();
	
	// Request to do this again ASAP
	requestAnimationFrame(main);
};

//get constant time
function timestamp() {

	return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
}

// Cross-browser support for requestAnimationFrame
var w = window;
requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

// Let's play this game!
var then = Date.now();
loadResources();
requestAnimationFrame(main);
